package com.example.foodguide.profile;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.foodguide.R;
import com.example.foodguide.database.model.User;


public class ProfileFragment extends Fragment {
    private View view;
private TextView txtProfile, txtName, txtPhone, txtEmail;
    private Intent mIntent = null;
    private User user = null;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        mIntent = getActivity().getIntent();
        if (mIntent != null) {
            user = (User) mIntent.getSerializableExtra("user");
            initView();
            txtProfile.setText(user.getFullName());
            txtName.setText(user.getFullName());
            txtPhone.setText(user.getPhone());
            txtEmail.setText(user.getEmail());
        }
        return view;
    }

    private void initView() {
        txtEmail = view.findViewById(R.id.tv_email_profile);
        txtName = view.findViewById(R.id.tv_fullname);
        txtPhone = view.findViewById(R.id.tv_phone_profile);
        txtProfile = view.findViewById(R.id.tv_name);
    }
}
