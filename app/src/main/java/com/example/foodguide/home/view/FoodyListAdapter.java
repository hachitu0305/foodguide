package com.example.foodguide.home.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.foodguide.R;
import com.example.foodguide.database.model.Foody;

import java.util.ArrayList;

public class FoodyListAdapter extends BaseAdapter {

    private Context context;
    private  int layout;
    private ArrayList<Foody> foodsList;

    public FoodyListAdapter(Context context, int layout, ArrayList<Foody> foodsList) {
        this.context = context;
        this.layout = layout;
        this.foodsList = foodsList;
    }

    @Override
    public int getCount() {
        return foodsList.size();
    }

    @Override
    public Object getItem(int position) {
        return foodsList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{
        ImageView imgItem;
        TextView txtTitle, tv_item_des;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View row = view;
        ViewHolder holder = new ViewHolder();

        if(row == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);

            holder.txtTitle = (TextView) row.findViewById(R.id.tv_item_title);
            holder.tv_item_des = (TextView) row.findViewById(R.id.tv_item_des);
            holder.imgItem = (ImageView) row.findViewById(R.id.img_item_new_service);
            row.setTag(holder);
        }
        else {
            holder = (ViewHolder) row.getTag();
        }

        Foody food = foodsList.get(position);

        holder.txtTitle.setText(food.getTenmonan());
        holder.tv_item_des.setText(food.getMota());

        byte[] foodImage = food.getHinhanh();
        Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);
        holder.imgItem.setImageBitmap(bitmap);

        return row;
    }
}
