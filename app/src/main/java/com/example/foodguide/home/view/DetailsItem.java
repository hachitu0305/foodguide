package com.example.foodguide.home.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.foodguide.R;
import com.example.foodguide.database.model.Foody;

public class DetailsItem extends AppCompatActivity {
    TextView tenmonan, mota;
    ImageView imageViewFood;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foody_detail);

        tenmonan = findViewById(R.id.tv_title_detail);
        mota = findViewById(R.id.tv_description_detail);
        imageViewFood = findViewById(R.id.img_spa_detail);

        Intent itFoody = getIntent();
        final Foody foody = (Foody) itFoody.getExtras().getSerializable("objFoody");

        tenmonan.setText(foody.getTenmonan());
        mota.setText(foody.getHuongdan());

        byte[] foodImage = foody.getHinhanh();
        if (foodImage != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);
            imageViewFood.setImageBitmap(bitmap);
        }
    }
}
