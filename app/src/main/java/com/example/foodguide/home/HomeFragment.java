package com.example.foodguide.home;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.foodguide.R;
import com.example.foodguide.database.model.Foody;
import com.example.foodguide.home.view.DetailsItem;
import com.example.foodguide.home.view.FoodyListAdapter;
import com.example.foodguide.home.view.SliderAdapter;
import com.example.foodguide.session.Session;
import com.example.foodguide.utils.Constant;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.IndicatorView.draw.controller.DrawController;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;

public class HomeFragment extends Fragment {
    private View view;
    SliderView sliderView;

    private ArrayList<Foody> foodys;
    private FoodyListAdapter adapter = null;
    private GridView gridView;
    private SQLiteDatabase db;

    private Session session;

    public HomeFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        session = new Session(getActivity());

        gridView = (GridView) view.findViewById(R.id.gridView);
        foodys = new ArrayList<>();
        adapter = new FoodyListAdapter(getActivity(), R.layout.item_food, foodys);
        gridView.setAdapter(adapter);

        // Mở cơ sở dữ liệu hiện có
        db = getActivity().openOrCreateDatabase(Constant.DATABASE_NAME, Context.MODE_PRIVATE, null);
        foodys.clear();

        try {
            Cursor c = db.rawQuery("SELECT * FROM monan",null);
            while (c.moveToNext()) {
                foodys.add(new Foody(
                        c.getInt(0),
                        c.getString(1), //ten mon an
                        c.getString(3), // MO ta
                        c.getString(2), // diachi
                        c.getString(5), // Huong dan
                        c.getBlob(7), // Hinh anh
                        c.getString(4), // Gia tien
                        c.getString(6) // Danh muc
                ));
            }
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Foody foody = (Foody) gridView.getItemAtPosition(position);
                Intent itFoody = new Intent(getActivity().getApplicationContext(), DetailsItem.class);
                itFoody.putExtra("objFoody", foody);
                startActivity(itFoody);
            }
        });

        adapter.notifyDataSetChanged();

        initView();
        return view;
    }

    private void initView() {
        sliderView = view.findViewById(R.id.imageSlider);
        final SliderAdapter adapter = new SliderAdapter(getActivity());
        adapter.setCount(5);
        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.SLIDE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINROTATIONTRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_BACK_AND_FORTH);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.startAutoCycle();

        sliderView.setOnIndicatorClickListener(new DrawController.ClickListener() {
            @Override
            public void onIndicatorClicked(int position) {
                sliderView.setCurrentPagePosition(position);
            }
        });
    }

}
