package com.example.foodguide.login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.foodguide.R;
import com.example.foodguide.database.createdatabase.SqliteHelper;
import com.example.foodguide.database.model.User;
import com.example.foodguide.home.HomeActivity;
import com.example.foodguide.register.RegisterActivity;
import com.example.foodguide.session.Session;


public class LoginActivity extends AppCompatActivity {
    private ImageButton mBtnLogin;
    private Button btnRegister;
    private static final int REQUEST_LOGIN = 0;
    SqliteHelper sqliteHelper;private
    Session session;

    public static EditText mEdtEmail, mEdtPassword;
    private final View.OnClickListener onClickLayout = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
            startActivity(intent);
        }
    };
    private final View.OnClickListener onClickLogin = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            actionLogin();
        }
    };

    private void actionLogin() {
        if (!isValidate()) {
            onLoginFailed();
            return;
        }
        mBtnLogin.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();

        String email = mEdtEmail.getText().toString();
        String password = mEdtPassword.getText().toString();
        final User user = sqliteHelper.getUserName(new User(null, null, null, email, password));
        if (user != null) {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            // On complete call either onLoginSuccess or onLoginFailed
                            onLoginSuccess();
                            progressDialog.dismiss();
                            changeScreen(user);
                        }
                    }, 2000);
        } else {
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            // On complete call either onLoginSuccess or onLoginFailed
                            onLoginFailed();
                            // onLoginFailed();
                            progressDialog.dismiss();
                        }
                    }, 2000);

        }
    }

    private void changeScreen(User user) {
        session.setIdUser(user.getId());
        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
        intent.putExtra("user", user);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_LOGIN) {
            if (resultCode == RESULT_OK) {
                this.finish();
            }
        }
    }


    public boolean isValidate() {
        boolean valid = true;

        String email = mEdtEmail.getText().toString();
        String password = mEdtPassword.getText().toString();

        if (email.isEmpty() || !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEdtEmail.setError("enter a valid email address");
            valid = false;
        } else {
            mEdtEmail.setError(null);
        }

        if (password.isEmpty() || password.length() < 8 || password.length() > 32) {
            mEdtPassword.setError("Password từ 8 đên 32 ký tự trở lên");
            valid = false;
        } else {
            mEdtPassword.setError(null);
        }

        return valid;
    }

    public void onLoginSuccess() {
        mBtnLogin.setEnabled(true);
        Toast.makeText(getBaseContext(), "Login success", Toast.LENGTH_LONG).show();
        finish();
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Please check email or password your again  !", Toast.LENGTH_LONG).show();
        mBtnLogin.setEnabled(true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        sqliteHelper = new SqliteHelper(this);
        session = new Session(this);
        initView();
    }

    private void initView() {
        mBtnLogin = findViewById(R.id.img_singin);
        mEdtEmail = findViewById(R.id.email);
        mEdtPassword = findViewById(R.id.password);
        btnRegister = findViewById(R.id.btn_singup);
        mBtnLogin.setOnClickListener(onClickLogin);
        btnRegister.setOnClickListener(onClickLayout);

    }
}
