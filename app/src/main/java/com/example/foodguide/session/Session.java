package com.example.foodguide.session;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Session {
    private SharedPreferences prefs;

    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void setIdUser(String idUser) {
        prefs.edit().putString("id_user", idUser).commit();
    }

    public String getIdUser() {
        String idUser = prefs.getString("id_user","");
        return idUser;
    }
}
