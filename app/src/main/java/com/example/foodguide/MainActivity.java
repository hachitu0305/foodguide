package com.example.foodguide;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.foodguide.database.crud.Insert;
import com.example.foodguide.database.crud.ListAll;
import com.example.foodguide.home.HomeActivity;
import com.example.foodguide.home.HomeFragment;


public class MainActivity extends AppCompatActivity {

    // Khai báo button
    Button btInsert, btList, btnHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Liên kết nút chèn và định cấu hình sự kiện nhấp để mở màn hình thêm mới
        btInsert = findViewById(R.id.btMainInsert);
        btInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent insert = new Intent(getApplicationContext(), Insert.class);
                startActivity(insert);
            }
        });

        //  Liên kết nút chèn và định cấu hình sự kiện nhấp để mở màn hình danh sách
        btList = findViewById(R.id.btMainList);
        btList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Tạo một hộp thông báo và hiển thị dữ liệu đi kèm
                Intent insert = new Intent(getApplicationContext(), ListAll.class);
                startActivity(insert);
            }
        });

        //  Liên kết nút chèn và định cấu hình sự kiện nhấp để mở màn hình danh sách
        btnHome = findViewById(R.id.btnHome);
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Tạo một hộp thông báo và hiển thị dữ liệu đi kèm
                Intent insert = new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(insert);
            }
        });

    }
}
