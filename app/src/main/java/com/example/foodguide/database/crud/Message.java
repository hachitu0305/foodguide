package com.example.foodguide.database.crud;

import android.app.AlertDialog;
import android.content.Context;

/**
 * Created by Edson Melo de Souza on 18/10/2017.
 */
public class Message {
    private Context _context;

    public Message(Context context) {
        this._context = context;
    }

    // Hiển thị thông báo
    public void show(String titulo, final String texto, int icone) {
        AlertDialog.Builder msg = new AlertDialog.Builder(_context);
        msg.setIcon(icone);
        msg.setTitle(titulo);
        msg.setMessage(texto);
        msg.setPositiveButton("OK", null);

        msg.show();

    }
}
