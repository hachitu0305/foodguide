package com.example.foodguide.database.crud;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.foodguide.R;
import com.example.foodguide.database.model.Foody;
import com.example.foodguide.session.Session;
import com.example.foodguide.utils.Constant;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class Insert extends AppCompatActivity {
    EditText tenmonan, diachi, mota, giatien, huongdan, hinhanh;
    private Spinner danhmuc;
    Button btInserir, btnChoose;
    SQLiteDatabase db;
    ImageView imageView;
    String messFail;
    Session session;

    final int REQUEST_CODE_GALLERY = 999;
    private static final String[] paths = {"Đồ ăn vặt", "Đồ ăn ngon", "Đồ ăn dở tệ"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);
        session = new Session(this);

        // Mở hoặc tạo cơ sở dữ liệu
        db = openOrCreateDatabase(Constant.DATABASE_NAME, Context.MODE_PRIVATE, null);

        // Tạo bảng nếu nó không tồn tại, nếu không thì tải bảng để sử dụng
        db.execSQL("CREATE TABLE IF NOT EXISTS monan(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "tenmonan VARCHAR NOT NULL, " +
                "diachi VARCHAR NOT NULL, " +
                "motamonan VARCHAR NOT NULL, " +
                "giatien VARCHAR NOT NULL, " +
                "huongdan VARCHAR NOT NULL, " +
                "danhmuc VARCHAR, " +
                "hinhanh BLOG, " +
                "id_user VARCHAR NOT NULL);");

        init();

        btnChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(
                        Insert.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY
                );
            }
        });

        btInserir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                add();
            }
        });
    }
    public void add(){

        if (!isValidate()) {
            // Tạo một hộp thông báo
            Message message = new Message(Insert.this);
            message.show(
                    "Thêm giới thiệu món ăn thất bại",
                    messFail,
                    R.drawable.ic_add);
            return;
        }

        // Tạo một đối tượng Sinh viên để nhận dữ liệu
        Foody foody = new Foody();
        foody.setTenmonan(tenmonan.getText().toString());
        foody.setDiachi(diachi.getText().toString());
        foody.setMota(mota.getText().toString());
        foody.setGiatien(giatien.getText().toString());
        foody.setHuongdan(huongdan.getText().toString());
        foody.setHinhanh(imageViewToByte(imageView));

        // Thu thập dữ liệu được nhập vào các trường
        ContentValues values = new ContentValues();
        values.put("tenmonan", foody.getTenmonan());
        values.put("motamonan", foody.getMota());
        values.put("diachi", foody.getDiachi());
        values.put("huongdan", foody.getHuongdan());
        values.put("hinhanh", foody.getHinhanh());
        values.put("giatien", foody.getGiatien());
        values.put("id_user", session.getIdUser());

        // Chèn dữ liệu vào bảng
        db.insert("monan", null, values);

        // Tạo một hộp thông báo và hiển thị dữ liệu bao gồm
        Message message = new Message(Insert.this);
        message.show(
                "Thêm mới dữ liệu thành công",
                foody.toString(),
                R.drawable.ic_add);

        // Xóa các trường đầu vào
        clearText();
    }

    private boolean isValidate() {
        boolean valid = true;

        if (tenmonan.getText().toString().isEmpty() || tenmonan.getText().toString().length() < 3) {
            tenmonan.setError("Tên món ăn phải có ít nhất 3 ký tự");
            messFail += "Tên món ăn phải có ít nhất 3 ký tự\n";
            valid = false;
        }

        if (diachi.getText().toString().isEmpty() || diachi.getText().toString().length() < 3) {
            diachi.setError("Địa chỉ phải có ít nhất 3 ký tự");
            messFail += "Địa chỉ phải có ít nhất 3 ký tự\n";
            valid = false;
        }

        if (mota.getText().toString().isEmpty() || mota.getText().toString().length() < 3) {
            mota.setError("Mô tả phải có ít nhất 3 ký tự");
            messFail += "Mô tả phải có ít nhất 3 ký tự\n";
            valid = false;
        }

        if (huongdan.getText().toString().isEmpty()) {
            huongdan.setError("Hướng dẫn không được để trống");
            messFail += "Hướng dẫn không được để trống\n";
            valid = false;
        }

        if (giatien.getText().toString().isEmpty()) {
            giatien.setError("Giá tiền không được để trống");
            messFail += "Giá tiền không được để trống\n";
            valid = false;
        }

        if (imageView==null) {
            hinhanh.setError("Hình ảnh không được để trống");
            messFail += "Hình ảnh không được để trống\n";
            valid = false;
        }

        return valid;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CODE_GALLERY){
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            }
            else {
                Toast.makeText(getApplicationContext(), "You don't have permission to access file location!", Toast.LENGTH_SHORT).show();
            }
            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null){
            Uri uri = data.getData();

            try {

                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imageView.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public static byte[] imageViewToByte(ImageView image) {
        Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    // Định cấu hình nút (mũi tên) trên ActionBar (Thanh trên cùng)
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Xóa các trường nhập và đóng bàn phím
     */
    public void clearText() {
        tenmonan.setText("");
        diachi.setText("");
        mota.setText("");
        giatien.setText("");
        huongdan.setText("");
        tenmonan.requestFocus();
        imageView.setImageResource(R.mipmap.ic_launcher);

        // đóng bàn phím ảo
        ((InputMethodManager) Insert.this.getSystemService(
                Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(
                getCurrentFocus().getWindowToken(), 0);
    }

    public void init(){
        tenmonan = findViewById(R.id.editTenmonan);
        diachi = findViewById(R.id.editDiachi);
        mota = findViewById(R.id.editMota);
        giatien = findViewById(R.id.editGiatien);
        huongdan = findViewById(R.id.editHuongdan);
        danhmuc = findViewById(R.id.slcDanhmuc);

        btInserir = findViewById(R.id.btInserir);
        btnChoose = findViewById(R.id.btnChoose);

        imageView = findViewById(R.id.imageView);
    }
}
