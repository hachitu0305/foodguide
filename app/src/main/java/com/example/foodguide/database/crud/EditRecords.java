package com.example.foodguide.database.crud;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.foodguide.MainActivity;
import com.example.foodguide.R;
import com.example.foodguide.database.model.Foody;
import com.example.foodguide.session.Session;
import com.example.foodguide.utils.Constant;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

public class EditRecords extends AppCompatActivity {
    TextView id;
    EditText tenmonan, diachi, mota, giatien, huongdan;
    ImageView imageViewEdit;
    Button btSalvar,btnChooseEdit;
    String messFail;
    Session session;

    final int REQUEST_CODE_GALLERY = 999;

    SQLiteDatabase db;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);
        session = new Session(this);

        init();

        final Intent itFoody = getIntent();
        final Foody foody = (Foody) itFoody.getExtras().getSerializable("objFoody");
        id.setText(String.valueOf(foody.getId()));
        tenmonan.setText(foody.getTenmonan());
        mota.setText(foody.getMota());
        diachi.setText(foody.getDiachi());
        huongdan.setText(foody.getHuongdan());
        giatien.setText(foody.getGiatien());

        byte[] foodImage = foody.getHinhanh();
        if (foodImage != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);
            imageViewEdit.setImageBitmap(bitmap);
        }

        btnChooseEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.requestPermissions(
                        EditRecords.this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        REQUEST_CODE_GALLERY
                );
            }
        });

        btSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isValidate()) {
                    // Tạo một hộp thông báo
                    Message message = new Message(EditRecords.this);
                    message.show(
                            "Cập nhật giới thiệu món ăn thất bại",
                            messFail,
                            R.drawable.ic_add);
                    return;
                }
                // Thu thập dữ liệu được nhập vào các trường
                ContentValues values = new ContentValues();
                values.put("id", foody.getId());
                values.put("tenmonan", foody.getTenmonan());
                values.put("diachi", foody.getDiachi());
                values.put("motamonan", foody.getMota());
                values.put("giatien", foody.getGiatien());
                values.put("huongdan", foody.getHuongdan());
                values.put("hinhanh", foody.getHinhanh());
                values.put("id_user", session.getIdUser());

                // Tạo một đối tượng Sinh viên để nhận dữ liệu
                Foody foodyUpdate = new Foody();
                foodyUpdate.setId(Integer.parseInt(id.getText().toString()));
                foodyUpdate.setTenmonan(tenmonan.getText().toString());
                foodyUpdate.setDiachi(diachi.getText().toString());
                foodyUpdate.setMota(mota.getText().toString());
                foodyUpdate.setGiatien(giatien.getText().toString());
                foodyUpdate.setHuongdan(huongdan.getText().toString());
                foodyUpdate.setHinhanh(imageViewToByte(imageViewEdit));

                ContentValues valuesUpdate = new ContentValues();
                valuesUpdate.put("id", foodyUpdate.getId());
                valuesUpdate.put("tenmonan", foodyUpdate.getTenmonan());
                valuesUpdate.put("diachi", foodyUpdate.getDiachi());
                valuesUpdate.put("motamonan", foodyUpdate.getMota());
                valuesUpdate.put("giatien", foodyUpdate.getGiatien());
                valuesUpdate.put("hinhanh", foodyUpdate.getHinhanh());
                valuesUpdate.put("huongdan", foodyUpdate.getHuongdan());
                valuesUpdate.put("id_user", session.getIdUser());

                // Cập nhật dữ liệu trong bảng
                db = openOrCreateDatabase(Constant.DATABASE_NAME, Context.MODE_PRIVATE, null);

                db.update("monan",valuesUpdate,"id="+foodyUpdate.getId()+"",null);

                Message message = new Message(EditRecords.this);
                // Tạo một hộp thông báo và hiển thị dữ liệu bao gồm
                message.show(
                        "Cập nhật giới thiệu món ăn thành công!",
                        foodyUpdate.toString(),
                        R.drawable.ic_add);
                ;
                Intent main = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(main);
            }
        });
    }

    private boolean isValidate() {
        boolean valid = true;

        if (tenmonan.getText().toString().isEmpty() || tenmonan.getText().toString().length() < 3) {
            tenmonan.setError("Tên món ăn phải có ít nhất 3 ký tự");
            messFail += "Tên món ăn phải có ít nhất 3 ký tự\n";
            valid = false;
        }

        if (diachi.getText().toString().isEmpty() || diachi.getText().toString().length() < 3) {
            diachi.setError("Địa chỉ phải có ít nhất 3 ký tự");
            messFail += "Địa chỉ phải có ít nhất 3 ký tự\n";
            valid = false;
        }

        if (mota.getText().toString().isEmpty() || mota.getText().toString().length() < 3) {
            mota.setError("Mô tả phải có ít nhất 3 ký tự");
            messFail += "Mô tả phải có ít nhất 3 ký tự\n";
            valid = false;
        }

        if (huongdan.getText().toString().isEmpty()) {
            huongdan.setError("Hướng dẫn không được để trống");
            messFail += "Hướng dẫn không được để trống\n";
            valid = false;
        }

        if (giatien.getText().toString().isEmpty()) {
            giatien.setError("Giá tiền không được để trống");
            messFail += "Giá tiền không được để trống\n";
            valid = false;
        }

        if (imageViewEdit==null) {
            messFail += "Hình ảnh không được để trống\n";
            valid = false;
        }

        return valid;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CODE_GALLERY){
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE_GALLERY);
            }
            else {
                Toast.makeText(getApplicationContext(), "You don't have permission to access file location!", Toast.LENGTH_SHORT).show();
            }
            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_CODE_GALLERY && resultCode == RESULT_OK && data != null){
            Uri uri = data.getData();

            try {

                InputStream inputStream = getContentResolver().openInputStream(uri);
                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imageViewEdit.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    public static byte[] imageViewToByte(ImageView image) {
        if (image!=null) {
            Bitmap bitmap = ((BitmapDrawable)image.getDrawable()).getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            return byteArray;
        }
        return null;
    }

    public void init(){
        id = findViewById(R.id.id);
        tenmonan = findViewById(R.id.tenmonan);
        diachi = findViewById(R.id.diachi);
        mota = findViewById(R.id.motamonan);
        giatien = findViewById(R.id.giatien);
        huongdan = findViewById(R.id.huongdan);
        btSalvar = findViewById(R.id.btSalvar);
        imageViewEdit = findViewById(R.id.imageViewEdit);
        btnChooseEdit = findViewById(R.id.btnChooseEdit);
    }

    // Định cấu hình nút (mũi tên) trên ActionBar (Thanh trên cùng)
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
