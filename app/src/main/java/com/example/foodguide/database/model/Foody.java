package com.example.foodguide.database.model;

import java.io.Serializable;

public class Foody implements Serializable {
    private int id;
    private String tenmonan;
    private String mota;
    private String diachi;
    private String huongdan;
    private String giatien;
    private String danhmuc;
    private byte[] hinhanh;

    public Foody() {
    }

    public Foody(int id, String tenmonan, String mota, String diachi, String huongdan, byte[] hinhanh, String giatien, String danhmuc ) {
        this.id = id;
        this.tenmonan = tenmonan;
        this.mota = mota;
        this.diachi = diachi;
        this.huongdan = huongdan;
        this.giatien = giatien;
        this.danhmuc = danhmuc;
        this.hinhanh = hinhanh;
    }

    public byte[] getHinhanh() {
        return hinhanh;
    }

    public void setHinhanh(byte[] hinhanh1) {
        this.hinhanh = hinhanh1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTenmonan() {
        return tenmonan;
    }

    public void setTenmonan(String tenmonan) {
        this.tenmonan = tenmonan;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }

    public String getHuongdan() {
        return huongdan;
    }

    public void setHuongdan(String huongdan) {
        this.huongdan = huongdan;
    }

    public String getGiatien() {
        return giatien;
    }

    public void setGiatien(String giatien) {
        this.giatien = giatien;
    }

    public String getDanhmuc() {
        return danhmuc;
    }

    public void setDanhmuc(String danhmuc) {
        this.danhmuc = danhmuc;
    }

    @Override
    public String toString() {
        return  "Tên món ăn: " + tenmonan + '\n' +
                "Mô tả món ăn:" + mota + '\n' +
                "Địa chỉ: " + diachi + '\n' +
                "Hướng dẫn: " + huongdan + '\n' +
                "Giá tiền: " + giatien + '\n' +
                "Danh mục: " + danhmuc + '\n';
    }
}
