package com.example.foodguide.database.crud;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.foodguide.MainActivity;
import com.example.foodguide.R;
import com.example.foodguide.database.model.Foody;
import com.example.foodguide.utils.Constant;

public class Details extends AppCompatActivity {
    Button btEditar, btnDelete;
    TextView tenmonan, mota, diachi, huongdan, giatien;
    ImageView imageViewFood;;
    SQLiteDatabase db;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        // Mở cơ sở dữ liệu hiện có
        db = openOrCreateDatabase(Constant.DATABASE_NAME, Context.MODE_PRIVATE, null);

        init();

        Intent itFoody = getIntent();
        final Foody foody = (Foody) itFoody.getExtras().getSerializable("objFoody");

        tenmonan.setText(foody.getTenmonan());
        mota.setText(foody.getMota());
        diachi.setText(foody.getDiachi());
        huongdan.setText(foody.getHuongdan());
        giatien.setText(foody.getGiatien());

        byte[] foodImage = foody.getHinhanh();
        if (foodImage != null) {
            Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);
            imageViewFood.setImageBitmap(bitmap);
        }

        btEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent editar = new Intent(getApplicationContext(), EditRecords.class);
                editar.putExtra("objFoody", foody);
                startActivity(editar);
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogDelete(foody.getId());
            }
        });
    }

    // Định cấu hình nút (mũi tên) trên ActionBar (Thanh trên cùng)
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void showDialogDelete(final int idFood){
        final AlertDialog.Builder dialogDelete = new AlertDialog.Builder(this);

        dialogDelete.setTitle("Warning!!");
        dialogDelete.setMessage("Bạn có thực sự muốn xóa dữ liệu giới thiệu món ăn này không ?");
        dialogDelete.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
//                    Cursor c = db.rawQuery("DELETE FROM monan WHERE id = ?", new String[]{String.valueOf(idFood)});
                    String sql = "DELETE FROM monan WHERE id = ?";
                    SQLiteStatement statement = db.compileStatement(sql);
                    statement.clearBindings();
                    statement.bindDouble(1, (double)idFood);
                    statement.execute();
                    Toast.makeText(getApplicationContext(), "Xóa dữ liệu thành công!!!",Toast.LENGTH_SHORT).show();
                } catch (Exception e){
                    Log.e("error", e.getMessage());
                }
                Intent main = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(main);
            }
        });

        dialogDelete.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        dialogDelete.show();
    }

    public void init(){
        tenmonan = findViewById(R.id.tenmonan);
        mota = findViewById(R.id.motamonan);
        diachi = findViewById(R.id.diachi);
        huongdan = findViewById(R.id.huongdan);
        giatien = findViewById(R.id.giatien);
        btEditar = findViewById(R.id.btSalvar);
        btnDelete = findViewById(R.id.btnXoa);
        imageViewFood = findViewById(R.id.imageViewDetail);
    }
}
