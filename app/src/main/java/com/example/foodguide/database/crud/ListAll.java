package com.example.foodguide.database.crud;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.foodguide.R;
import com.example.foodguide.database.model.Foody;
import com.example.foodguide.session.Session;
import com.example.foodguide.utils.Constant;

import java.util.ArrayList;

public class ListAll extends AppCompatActivity {
    ListView listViewFoody;
    ArrayList<Foody> foody = new ArrayList<>();
    ArrayAdapter<Foody> adaptador;
    SQLiteDatabase db;
    Session session;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        // Mở cơ sở dữ liệu hiện có
        db = openOrCreateDatabase(Constant.DATABASE_NAME, Context.MODE_PRIVATE, null);
        listViewFoody = findViewById(R.id.listagem);
        session = new Session(this);

        // Tải các bản ghi theo thứ tự abc trong ArrayList để đính kèm với bộ chuyển đổi
        foody.clear();
        Cursor c = db.rawQuery("SELECT * FROM monan WHERE id_user = ?", new String[]{session.getIdUser()});
        while (c.moveToNext()) {
            foody.add(new Foody(
                    c.getInt(0),
                    c.getString(1), //ten mon an
                    c.getString(3), // MO ta
                    c.getString(2), // diachi
                    c.getString(5), // Huong dan
                    c.getBlob(7), // Hinh anh
                    c.getString(4), // Gia tien
                    c.getString(6) // Danh muc
            ));
        }
        // Cấu hình bộ chuyển đổi
        adaptador = new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                foody);


        // Đính kèm bộ chuyển đổi vào ListView
        listViewFoody.setAdapter(adaptador);

        listViewFoody.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Foody foody = (Foody) listViewFoody.getItemAtPosition(position);
                Intent itFoody = new Intent(getApplicationContext(), Details.class);
                itFoody.putExtra("objFoody", foody);
                startActivity(itFoody);
            }
        });
    }

    // Định cấu hình nút (mũi tên) trên ActionBar (Thanh trên cùng)
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
