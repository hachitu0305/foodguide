package com.example.foodguide.register;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.example.foodguide.R;
import com.example.foodguide.database.createdatabase.SqliteHelper;
import com.example.foodguide.database.model.User;
import com.example.foodguide.login.LoginActivity;
import com.example.foodguide.profile.ProfileFragment;


public class RegisterActivity extends AppCompatActivity {
    private Button mBtnLogin;
    private ImageButton mBtnSingup;

    private EditText mEdtFullName, mEdtEmail, mEdtPassword, mEdtPhone;
    SqliteHelper sqliteHelper;

    private final View.OnClickListener onClickSuccess = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            actionRegister();
        }
    };

    private View.OnClickListener onClickListener = new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
    }
};

    @Override
    public void supportInvalidateOptionsMenu() {
        super.supportInvalidateOptionsMenu();
    }

    @Override
    public void invalidateOptionsMenu() {
        super.invalidateOptionsMenu();
    }

    private void actionRegister() {
        if (!isValidate()) {
            onSignupFailed();
            return;
        }
        mBtnSingup.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this,
                R.style.Theme_AppCompat_DayNight_Dialog);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        final String name = mEdtFullName.getText().toString();
        final String email = mEdtEmail.getText().toString();
        String password = mEdtPassword.getText().toString();
        final String phone = mEdtPhone.getText().toString();
        if (!sqliteHelper.isEmailExists(email)) {
            sqliteHelper.addUser(new User(null, name, phone, email, password));
            new android.os.Handler().postDelayed(
                    new Runnable() {
                        public void run() {
                            onSignupSuccess();
                            progressDialog.dismiss();

                        }
                    }, 2000);
        } else {
            onSignupFailed();
        }
    }

//    private void senData(User user) {
//        Intent intent = new Intent(RegisterActivity.this, ProfileFragment.class);
//        intent.putExtra("user", user.getId());
//    }

    public void onSignupSuccess() {
        mBtnSingup.setEnabled(true);
        setResult(RESULT_OK, null);
        Toast.makeText(RegisterActivity.this, "Register success !", Toast.LENGTH_LONG).show();
        finish();
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Register failed !", Toast.LENGTH_LONG).show();
        mBtnSingup.setEnabled(true);
    }

    private boolean isValidate() {
        boolean valid = true;
        String name = mEdtFullName.getText().toString();
        String email = mEdtEmail.getText().toString();
        String password = mEdtPassword.getText().toString();
        String phone = mEdtPhone.getText().toString();
        if (name.isEmpty() || name.length() < 3) {
            mEdtFullName.setError("at least 3 characters");
            valid = false;
        } else {
            mEdtFullName.setError(null);
        }

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEdtEmail.setError("enter a valid email address");
            valid = false;
        } else {
            mEdtEmail.setError(null);
        }

        if (password.isEmpty() || password.length() < 8 || password.length() > 32) {
            mEdtPassword.setError("between 8 and 32 alphanumeric characters");
            valid = false;
        } else {
            mEdtPassword.setError(null);
        }
        if (phone.isEmpty() || !Patterns.PHONE.matcher(phone).matches()) {
            mEdtPhone.setError(" enter a valid phone");
            valid = false;
        } else {
            mEdtPhone.setError(null);
        }

        return valid;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        sqliteHelper = new SqliteHelper(this);
        initView();
    }

    private void initView() {
        mBtnSingup = findViewById(R.id.img_singup);
        mEdtPhone = findViewById(R.id.edt_phone);
        mEdtFullName = findViewById(R.id.name);
        mEdtPassword = findViewById(R.id.password);
        mEdtEmail = findViewById(R.id.email);
        mBtnLogin = findViewById(R.id.btn_login);
        mBtnLogin.setOnClickListener(onClickListener);
        mBtnSingup.setOnClickListener(onClickSuccess);
    }
}
